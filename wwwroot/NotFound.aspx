<%@ Page Language="c#" AutoEventWireup="false" Inherits="BVNetwork.NotFound.Core.NotFoundPage.NotFoundBase" %>
<!doctype html>
<html class="no-js" lang="sv" syle="background:black">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=test">
        <title>Test</title>
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/gui/css/main.css" rel="stylesheet" type="text/css" media="screen">
    </head>
    <body>
        <div class="header-wrapper">
            <header>
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="/"><img src="/gui/images/testhr-logotype-neg.png" alt=""></a>
                        </div>
                    </div>
                </nav>
            </header>
        </div>
        <div class="wrapper main-article" style="background:white">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1>Sidan gick inte att hitta</h1>
                        <p>Sidan du f�rs�kte ladda var: <%= UrlNotFound %></p>
                        <p>Sidan kan ha f�rsvunnit, eller s� har den aldrig funnits.</p>
                        <p><a href="/">G� tillbaka till startsidan</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper footer">
            <footer>
                <div class="container">
                    <div class="center">
                        <a class="brand" href="#"><img src="/gui/images/testhr-logotype-neg.png" alt=""></a>
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>
<%-- 
    Note! This file has no code-behind. It inherits from the NotFoundBase class. You can 
    make a copy of this file into your own project, change the design and keep the inheritance 
    WITHOUT having to reference the BVNetwork.EPi404.dll assembly.
    
    If you want to use your own Master Page, inherit from SimplePageNotFoundBase instead of
    NotFoundBase, as that will bring in what is needed by EPiServer. Note! you do not need to
    create a page type for this 404 page. If you use the EPiServer API, and inherit from  
    SimplePageNotFoundBase, this page will run in the context of the site start page.
    
    Be very careful with the code you write here. If you reference resources that cannot be found
    you could end up in an infinite loop.
    
    The code behind file might do a redirect to a new page based on the redirect configuration if
    it matches the url not found. The Error event (where the rest of the redirection is done)
    might not run for .aspx files that are not found, instead it redirects here with the url it
    could not find in the query string.
    
    Available properties:
        Content (BVNetwork.FileNotFound.Content.PageContent)
            // Labels you can use - fetched from the language file
            Content.BottomText
            Content.CameFrom
            Content.LookingFor
            Content.TopText
            Content.Title
            
        UrlNotFound (string)
            The url that cannot be found
        
        Referer (string)
            The url that brought the user here
            It no referer, the string is empty (not null)
            
    If you are using a master page, you should add this:
        <meta content="noindex, nofollow" name="ROBOTS">
    to your head tag for this page (NOT all pages)
 --%>