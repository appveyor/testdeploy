﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace TestDeploy.Helpers
{
    public static class MailExtensions
    {
        // Add "~" support for pickupdirectories.
        public static void SetRelativePickupDirectoryLocation(this SmtpClient smtp)
        {
            string root = AppDomain.CurrentDomain.BaseDirectory;
            string pickupRoot = smtp.PickupDirectoryLocation.Replace("~/", root);
            pickupRoot = pickupRoot.Replace("/", @"\");
            smtp.PickupDirectoryLocation = pickupRoot;
        }

        public static void SetRecipients(this MailMessage message, string recipients)
        {
            var toAddresses = recipients.Split(';');
            if (toAddresses != null && toAddresses.Any())
            {
                foreach (string toAddress in toAddresses)
                {
                    message.To.Add(new MailAddress(toAddress.Trim()));
                }
            }
        }
    }
}