﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.SpecializedProperties;
using System.Collections;

namespace TestDeploy.Helpers
{
    public static class LinkItemCollectionExtensions
    {
        /// <summary>
        ///     Returns a PageDataCollection with all the EPiServer pages from a LinkItemCollection.
        /// </summary>
        /// <param name="linkItemCollection">Source LinkItemCollection to look for EPiServer pages.</param>
        /// <returns>PageDataCollection with EPiServer pages from a LinkItemCollection.</returns>
        public static PageDataCollection ToPageDataCollection(this LinkItemCollection linkItemCollection)
        {
            return linkItemCollection.ToEnumerable<PageData>().ToPageDataCollection();
        }

        /// <summary>
        ///     Returns a sequence with all the EPiServer pages of given type <typeparamref name="T" /> in a LinkItemCollection
        /// </summary>
        /// <param name="linkItemCollection">Source LinkItemCollection to look for EPiServer pages.</param>
        /// <returns>Sequence of the EPiServer pages of type <typeparamref name="T" /> in a LinkItemCollection</returns>
        public static IEnumerable<T> ToEnumerable<T>(this LinkItemCollection linkItemCollection)
            where T : PageData
        {
            if (linkItemCollection == null)
            {
                return Enumerable.Empty<T>();
            }

            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            return linkItemCollection
                .Select(x => x.ToContentReference())
                .Where(x => !x.IsNullOrEmpty())
                .Select(contentLoader.Get<IContent>)
                .SafeOfType<T>();
        }
        public static PageDataCollection ToPageDataCollection(this IEnumerable<PageData> pages)
        {
            return new PageDataCollection(pages);
        }
        public static bool IsNullOrEmpty(this ContentReference contentReference)
        {
            return ContentReference.IsNullOrEmpty(contentReference);
        }
        public static IEnumerable<T> SafeOfType<T>(this IEnumerable source)
        {
            return source == null ? Enumerable.Empty<T>() : source.OfType<T>().OrEmptyIfNull();
        }
        public static IEnumerable<T> OrEmptyIfNull<T>(this IEnumerable<T> source)
        {
            return source ?? Enumerable.Empty<T>();
        }
    }

}